import React, {useState} from 'react';
import {AppContainer, Heading} from "./app.styled";
import MyJqlEditor from "./components/MyJqlEditor";
import {useIssuesFromJql} from "./hooks/use-issues-from-jql";
import IssuesVisualiser from "./components/IssuesVisualiser";

export default function App() {
    const [jql, setJql] = useState('');
    const { loading, errors, issues } = useIssuesFromJql(jql);

    return (
        <AppContainer>
            <Heading>Issue status summary</Heading>
            <MyJqlEditor jql={jql} setJql={setJql} queryErrors={errors} />
            <IssuesVisualiser issues={issues} loading={loading} />
        </AppContainer>
    )
}
