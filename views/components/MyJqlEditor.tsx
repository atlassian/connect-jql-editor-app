import React, {useCallback} from 'react';
import { JQLEditorConnect } from "@atlassianlabs/jql-editor-connect";
import { Container } from './MyJqlEditor.styled';

type Props = {
    /**
     * The query to render in the editor.
     */
    jql: string,
    /**
     * Update JQL when the editor is searched.
     */
    setJql: (jql: string) => void,
    /**
     * Error messages when trying to retrieve issues.
     */
    queryErrors: { type: 'error', message: string }[],
}

const MyJqlEditor = ({ jql, setJql, queryErrors }: Props) => {
    const onSearch = useCallback((query, jast) => {
        // Only update our JQL if the query is valid
        if (jast.errors.length === 0) {
            setJql(query);
        }
    }, [setJql]);

    return (
        <Container>
            <JQLEditorConnect locale={"en"} query={jql} onSearch={onSearch} messages={queryErrors} />
        </Container>
    )
}

export default MyJqlEditor
