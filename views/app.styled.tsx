import styled from 'styled-components';

export const AppContainer = styled.div`
  padding: 32px;
  flex-grow: 1;
  display: flex;
  flex-direction: column;
`

export const Heading = styled.h3`
  margin-bottom: 16px;
`
