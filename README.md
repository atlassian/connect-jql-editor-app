# Connect JQL Editor App

[![Atlassian license](https://img.shields.io/badge/license-Apache%202.0-blue.svg?style=flat-square)](LICENSE) [![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](CONTRIBUTING.md)

This project contains a Connect app using atlassian-connect-express that displays a JQL editor in a Jira page.

![JQL Editor app for Jira](./docs/images/example.gif "JQL Editor for Jira")

## Requirements

[Read the docs](https://bitbucket.org/atlassian/atlassian-connect-express/src/master/README.md) to get started with 
atlassian-connect-express.

## Support

If you need help using Express.js, see the [API reference](https://expressjs.com/en/4x/api.html) or developer's guide.

If you need help developing against Atlassian products, see the [Atlassian Developer](https://developer.atlassian.com/) site.

If you need help using functionality provided by ACE, or would like to report a problem with the toolkit, please post in the
[Atlassian Developer Community](https://community.developer.atlassian.com/c/atlassian-developer-tools).

For developers outside of Atlassian looking for help with the JQL editor, or to report issues, [please make a post on the community forum](https://community.developer.atlassian.com/c/atlassian-ecosystem-design).
We will monitor the forums and redirect topics to the appropriate maintainers.

## Contributions

Contributions to Connect JQL Editor App are welcome! Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details.

## License

Copyright (c) 2021 - 2022 Atlassian and others.
Apache 2.0 licensed, see [LICENSE](LICENSE) file.

<br/>

[![With thanks from Atlassian](https://raw.githubusercontent.com/atlassian-internal/oss-assets/master/banner-with-thanks-light.png)](https://www.atlassian.com)
